# Todo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.1.

## Details

I selected Angular4 as front end application framework to develop TODO app. After app initial setup, I set the routes and app html structure to manage two modules:
Authentication
Tasks

In authentication module I used FirebaseAuth service to create user, login user && logout. After authentication user is redirected to home screen. Upon refresh page or coming back to application will restore the session and redirect the user to home. On home screen user will be able to create task, view all created tasks and delete task functionalities. From home he can logout to go back to authentication module. For storage I used FirebaseRealTimeDatabase service. 

After developing these features I used NodeJS framework Express and merged in the TODO angular app. And lastly I pushed the code to Heroku server with some scripts updates.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
