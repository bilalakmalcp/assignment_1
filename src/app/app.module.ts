import { BrowserModule } from '@angular/platform-browser';
import { NgModule, enableProdMode } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { PageNotFound } from './page_not_found';
import { Tasks } from './tasks/tasks.component';

import { AngularFireModule } from 'angularfire2';
import { MomentModule } from 'angular2-moment';
// New imports to update based on AngularFire2 version 4
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { Auth } from './auth/auth.component';
import { ReversePipe } from './tasks/reverse_pipe';
enableProdMode();

export const firebaseConfig = {
  apiKey: "AIzaSyDzAeQu7MOP28fbuNyfEJz7V4eaQcnTpsc",
  authDomain: "localhost",
  databaseURL: 'https://safarkhoush-1263.firebaseio.com/'
};

const appRoutes: Routes = [
  {
    path: 'tasks',
    component: Tasks,
    data: { title: 'Task List' }
  },
  {
    path: 'auth',
    component: Auth
  },
  {
    path: '',
    redirectTo: '/auth',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFound }
];

@NgModule({
  declarations: [
    AppComponent,
    Tasks,
    ReversePipe,
    Auth,
    PageNotFound
  ],
  imports: [
    FormsModule,
    MomentModule,
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RouterModule.forRoot(
      appRoutes,
      // { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [ReversePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
