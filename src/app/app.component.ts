import { Component } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public user: firebase.User;
  private logout_hover_flag: boolean;

  constructor(public afAuth: AngularFireAuth, private router: Router) {

    this.logout_hover_flag = false;
    this.afAuth.authState.subscribe(
      (auth) => {
        this.user = auth;
        if (!this.user)
          this.router.navigate(['/auth']);
        else
          this.logout_hover_flag = false;
      });
  }

  /**
   * Logout
   */
  logout(event) {
    if (event)
      event.stopPropagation();
    this.afAuth.auth.signOut()
      .then((res) => {
      }, (err) => {
      })
  }

  toggle_logout_flag() {
    this.logout_hover_flag = !this.logout_hover_flag;
  }

  hide_logout() {
    this.logout_hover_flag = false;
  }
}
