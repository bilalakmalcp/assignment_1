import { Component, OnInit, Pipe } from '@angular/core';
import { Router } from '@angular/router';
/**
 * Firebase Imports
 */
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';


@Component({
  selector: 'tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class Tasks implements OnInit {

  /**
   * Variables
   */
  private tasks_observable: FirebaseListObservable<any[]>;
  public tasks: any = [];
  public task_txt: string = '';
  public request_wait: boolean;
  private user: firebase.User;

  constructor(public afAuth: AngularFireAuth, private router: Router, public af: AngularFireDatabase) {
    this.request_wait = true;
  }

  ngOnInit(): void {
    this.afAuth.authState.subscribe(
      (auth) => {
        this.user = auth;
        if (this.user) {
          this.tasks_observable = this.af.list(this.user.uid + '/tasks', {
            query: {
              limitToLast: 50
            }
          });
          this.tasks_observable.subscribe((res) => {
            this.request_wait = false;
            this.tasks = res;
          })
        }
      });
  }


  /**
   * Methods
   */

  //Create Task
  create_new_task() {
    if (this.task_txt != '') {
      this.request_wait = true;
      var time = new Date().getTime();
      var _task = {
        text: this.task_txt,
        time: time,
        user_id: this.user.uid
      };
      this.tasks_observable.push(_task).then((res) => {
        this.task_txt = '';
        this.request_wait = false;
      }).catch((err) => {
        this.request_wait = false;
      });
    }
  }
  //Delete Task
  delete_task(_item) {
    this.tasks_observable.remove(_item)
  }

}
